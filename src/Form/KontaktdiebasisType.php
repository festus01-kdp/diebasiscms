<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class KontaktdiebasisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('anrede', ChoiceType::class, [
                'label' => 'Anrede',
                'choices' => [
                    'Frau' => 'Frau',
                    'Herr' => 'Herr',
                ],
                'data' => 'Herr',
            ])
            ->add('vorname', TextType::class, [
                'label' => 'Vorname',
            ])
            ->add('nachname', TextType::class, [
                'label' => 'Nachname',
            ])
            ->add('telefon', TelType::class, [
                'label' => 'Telefon',
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-Mail'
            ])
            ->add('nachricht', TextareaType::class, [
                'label' => 'Nachricht',
                'attr' => [
                    'rows' => '4',
                ],
            ])
//            ->add('newsletter', CheckboxType::class, [
//                'label' => 'Newsletter abbonieren',
//                'required' => false,
//            ])
            ->add('senden', SubmitType::class, [
                'label' => 'Absenden',
            ])
            ->add('abbruch', SubmitType::class, [
                'label' => 'Abbrechen',
                'attr' => [
                    'formnovalidate' => 'formnovalidate'
                ]
            ]);
//            ->add('reset', ResetType::class, [
//                'label' => 'Reset',
//        ]);
    }
}