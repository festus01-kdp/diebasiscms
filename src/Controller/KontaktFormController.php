<?php

namespace App\Controller;

use App\Form\KontaktdiebasisType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class KontaktFormController extends AbstractController
{
    public function showKontaktForm(Request $request, MailerInterface $mailer): Response
    {
        $kontaktFormular = $this->createForm(KontaktdiebasisType::class);

        $kontaktFormular->handleRequest($request);

        if ($kontaktFormular->getClickedButton() === $kontaktFormular->get('abbruch')){
            return $this->redirect('/');
        }

        if ($kontaktFormular->isSubmitted() && $kontaktFormular->isValid()) {
            if ($kontaktFormular->getClickedButton() === $kontaktFormular->get('senden')){

                $formDaten = $request->request->all();
//                var_dump($formDaten);
                // After send to dieBasis
                $this->sendHtmlemail($mailer, $formDaten);

                //return $this->redirect('mails/success_diebasis.html.twig', 302);
                return $this->render('mails/success_diebasis.html.twig', [
                    'kontaktdaten' => $formDaten
                ]);
            }

        }

        return $this->render('pages/kontaktdiebasis.html.twig', [
            'kontaktformular' => $kontaktFormular->createView()
        ]);

    }

    private function sendHtmlemail(MailerInterface $mailer, array $formDaten)
    {
//        dd($formdaten['email']);
        $email = (new TemplatedEmail())
            ->from('noreplay@diebasis-diepholz-nienburg.de')
            ->to(new Address($formDaten['kontaktdiebasis']['email']))
            ->subject('Anfrage Kontaktformular')

            ->htmlTemplate('mails/success_diebasis_mail.html.twig')

            ->context([
                'daten' => $formDaten['kontaktdiebasis']
            ])
        ;
        $mailer->send($email);
        // Formular für Admin
        $email = (new TemplatedEmail())
            ->from('noreplay@diebasis-diepholz-nienburg.de')
            ->to('klaus-dieter.pernak@diebasis-ni.de')
            ->subject('Anfrage Kontaktformular für Admin')

            ->htmlTemplate('mails/success_diebasis_mail_admin.html.twig')
            ->context([
                'daten' => $formDaten['kontaktdiebasis']
            ]);

        $mailer->send($email);
    }
}